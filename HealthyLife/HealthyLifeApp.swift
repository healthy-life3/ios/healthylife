//
//  HealthyLifeApp.swift
//  HealthyLife
//
//  Created by Артём on 25.10.2021.
//

import SwiftUI
import Swinject

@main
struct HealthyLifeApp: App {
    
    let dependencyResolver = Container().registerBindings()
    
    var body: some Scene {
        WindowGroup {
            ContentView(dependencyResolver: dependencyResolver)
        }
    }
}
