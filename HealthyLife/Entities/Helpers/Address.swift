//
//  Address.swift
//  HealthyLife
//
//  Created by Никита Гусев on 22.12.2021.
//

import Foundation

struct Address: Codable {
    let region: String
    let city: String
    let street: String
    let houseNumber: String
    let apartmentNumber: Int?
    let zipcode: String
}
