//
//  File.swift
//  HealthyLife
//
//  Created by Никита Гусев on 27.10.2021.
//

import Foundation

enum Gender: String, Codable {
    case male
    case female
    case unknown
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        let rawValue = try container.decode(String.self)
        
        self = Gender(rawValue: rawValue.lowercased()) ?? .unknown
    }
}
