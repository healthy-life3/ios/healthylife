//
//  Credentials.swift
//  HealthyLife
//
//  Created by Никита Гусев on 22.12.2021.
//

import Foundation

struct Credentials: Encodable {
    let password: String
}
