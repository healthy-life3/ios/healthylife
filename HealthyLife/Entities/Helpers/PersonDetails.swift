//
//  PersonDetails.swift
//  HealthyLife
//
//  Created by Никита Гусев on 22.12.2021.
//

import Foundation

struct PersonDetails: Encodable {
    let lastName: String
    let firstName: String
    let middleName: String?
    let gender: Gender
    let birthday: Date
    let phoneNumber: Phone
    let email: Email
}
