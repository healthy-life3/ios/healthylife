//
//  Phone.swift
//  HealthyLife
//
//  Created by Никита Гусев on 27.10.2021.
//

import Foundation

struct Phone: Codable {
    let fullNumber: String
    let countryCode: String
    let subscriberNumber: String
    
    init?(
        fullNumber: String,
        countryCode: String,
        subscriberNumber: String
    ) {
        self.fullNumber = fullNumber
        self.countryCode = countryCode
        self.subscriberNumber = subscriberNumber
        
        guard isValidated(fullNumber) else {
            return nil
        }
    }
    
    private func isValidated(_ fullNumber: String) -> Bool {
        return true
    }
}
