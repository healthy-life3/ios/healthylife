//
//  Passport.swift
//  HealthyLife
//
//  Created by Никита Гусев on 22.12.2021.
//

import Foundation

struct Passport: Codable {
    let documentSeries: String
    let documentNumber: String
    let dateOfIssue: Date
    let placeOfIssue: String
}
