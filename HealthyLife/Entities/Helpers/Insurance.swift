//
//  Insurance.swift
//  HealthyLife
//
//  Created by Никита Гусев on 22.12.2021.
//

import Foundation

struct Insurance: Codable {
    let snils: String
    
    init(snils: String) {
        self.snils = snils
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        snils = try container.decode(String.self)
    }
}
