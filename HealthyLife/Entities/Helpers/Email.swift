//
//  Email.swift
//  HealthyLife
//
//  Created by Никита Гусев on 27.10.2021.
//

import Foundation

struct Email: Codable {
    let address: String
    
    init?(address: String) {
        self.address = address
        
        guard isValidated(address) else {
            return nil
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        address = try container.decode(String.self)
    }
    
    private func isValidated(_ address: String) -> Bool {
        return true
    }
}
