//
//  PatientPersonalInfo.swift
//  HealthyLife
//
//  Created by Никита Гусев on 22.12.2021.
//

import Foundation

struct PatientPersonalInfo {
    let personDetails: PersonDetails
    let passport: Passport
    let insurance: Insurance
    let address: Address
}

// MARK: - PatientPersonalInfo decodable conformance
extension PatientPersonalInfo: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let firstName = try container.decode(String.self, forKey: .firstName)
        let lastName = try container.decode(String.self, forKey: .lastName)
        let middleName = try container.decode(String.self, forKey: .middleName)
        let gender = try container.decode(Gender.self, forKey: .gender)
        let birthday = try container.decode(Date.self, forKey: .birthday)
        let phoneNumber = try container.decode(Phone.self, forKey: .phoneNumber)
        let email = try container.decode(Email.self, forKey: .email)
        
        personDetails = PersonDetails(
            lastName: lastName,
            firstName: firstName,
            middleName: middleName,
            gender: gender,
            birthday: birthday,
            phoneNumber: phoneNumber,
            email: email
        )
        
        self.passport = try container.decode(Passport.self, forKey: .passport)
        self.insurance = try container.decode(Insurance.self, forKey: .snils)
        self.address = try container.decode(Address.self, forKey: .address)
    }
    
    enum CodingKeys: CodingKey {
        case firstName
        case lastName
        case middleName
        case gender
        case birthday
        case phoneNumber
        case email
        case passport
        case snils
        case address
    }
}
