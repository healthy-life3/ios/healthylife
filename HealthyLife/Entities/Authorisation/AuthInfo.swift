//
//  AuthInfo.swift
//  HealthyLife
//
//  Created by Никита Гусев on 27.10.2021.
//

import Foundation

struct AuthInfo: Encodable {
    let login: Email
    let password: String
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(login.address, forKey: .login)
        try container.encode(password, forKey: .password)
    }
    
    enum CodingKeys: CodingKey {
        case login
        case password
    }
}
