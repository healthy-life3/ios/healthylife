//
//  PatientRegistryInfo.swift
//  HealthyLife
//
//  Created by Никита Гусев on 27.10.2021.
//

import Foundation

struct RegistryInfo: Encodable {
    let personDetails: PersonDetails
    let passport: Passport
    let insurance: Insurance?
    let address: Address?
    let credentials: Credentials
    
    init(
        personDetails: PersonDetails,
        passport: Passport,
        insurance: Insurance? = nil,
        address: Address? = nil,
        credentials: Credentials
    ) {
        self.personDetails = personDetails
        self.passport = passport
        self.insurance = insurance
        self.address = address
        self.credentials = credentials
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(personDetails.firstName, forKey: .firstName)
        try container.encode(personDetails.lastName, forKey: .lastName)
        try container.encode(personDetails.middleName, forKey: .middleName)
        try container.encode(personDetails.gender.rawValue.uppercased(), forKey: .gender)
        try container.encode(personDetails.birthday, forKey: .birthday)
        try container.encode(personDetails.phoneNumber, forKey: .phoneNumber)
        try container.encode(personDetails.email.address, forKey: .email)
        try container.encode(passport, forKey: .passport)
        try container.encode(credentials.password, forKey: .password)
        
        if let insurance = insurance {
            try container.encode(insurance.snils, forKey: .snils)
        }
        
        if let address = address {
            try container.encode(address, forKey: .address)
        }
    }
}

// MARK: CodingKeys
extension RegistryInfo {
    enum CodingKeys: CodingKey {
        case firstName
        case lastName
        case middleName
        case gender
        case birthday
        case phoneNumber
        case email
        case passport
        case snils
        case address
        case password
    }
}
