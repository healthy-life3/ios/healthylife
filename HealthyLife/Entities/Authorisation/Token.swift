//
//  Token.swift
//  HealthyLife
//
//  Created by Никита Гусев on 02.12.2021.
//

import Foundation

struct Token: Decodable {
    let token: String
}
