//
//  MedicalRecord.swift
//  HealthyLife
//
//  Created by Никита Гусев on 21.12.2021.
//

import Foundation

struct MedicalRecord {
    let recordId: UUID
    let kind: MedicalRecord.Kind
    let title: String
    let date: Date
    let doctor: Doctor
    let medicalOrganization: String
}

// MARK: - Medical record helpers
extension MedicalRecord {
    enum Kind: String, Decodable {
        case lab = "LAB"
        case visit = "VISIT"
    }
    
    struct Doctor: Decodable {
        let lastName: String
        let firstName: String
        let middleName: String
    }
}

// MARK: - Medical record decodable conformance
extension MedicalRecord: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        recordId = try container.decode(UUID.self, forKey: .medicalRecordUuid)
        kind = try container.decode(MedicalRecord.Kind.self, forKey: .medicalRecordType)
        title = try container.decode(String.self, forKey: .title)
        date = try container.decode(Date.self, forKey: .dateTime)
        doctor = try container.decode(Doctor.self, forKey: .doctor)
        medicalOrganization = try container.decode(String.self, forKey: .medicalOrganization)
    }
    
    enum CodingKeys: CodingKey {
        case medicalRecordUuid
        case medicalRecordType
        case title
        case dateTime
        case doctor
        case medicalOrganization
    }
}
