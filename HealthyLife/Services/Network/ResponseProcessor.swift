//
//  ResponseProcessor.swift
//  HealthyLife
//
//  Created by Никита Гусев on 27.10.2021.
//

import Foundation

protocol ResponseProcessorType {
    func extractError(from response: URLResponse) -> Error?
}
