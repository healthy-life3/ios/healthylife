//
//  RegistryService.swift
//  HealthyLife
//
//  Created by Никита Гусев on 27.10.2021.
//

import Foundation
import RxSwift

// MARK: RegistryServiceType
protocol RegistryServiceType {
    func registerDoctor(
        personDetails: PersonDetails,
        passport: Passport,
        credentials: Credentials
    ) -> Single<Void>
    
    func registerPatient(
        personDetails: PersonDetails,
        passport: Passport,
        insurance: Insurance,
        address: Address,
        credentials: Credentials
    ) -> Single<Void>
}

// MARK: - Registry Error
extension RegistryService {
    enum Error: Swift.Error {
    }
}

// MARK: - Registry Response Processor
private final class RegistryResponseProcessor: ResponseProcessorType {
    func extractError(from response: URLResponse) -> Error? {
        nil
    }
}

// MARK: RegistryService
final class RegistryService: RegistryServiceType {
    private let networkService: NetworkServiceType
    private let responseProcessor: ResponseProcessorType
    
    init(networkService: NetworkServiceType) {
        self.networkService = networkService
        responseProcessor = RegistryResponseProcessor()
    }
    
    func registerDoctor(
        personDetails: PersonDetails,
        passport: Passport,
        credentials: Credentials
    ) -> Single<Void> {
        let registryInfo = RegistryInfo(
            personDetails: personDetails,
            passport: passport,
            credentials: credentials
        )
        
        let body = try? isoDateJsonEncoder.encode(registryInfo)
        
        networkService.dropToken()
        
        return networkService
            .fetchModel(
                path: "/doctors/register",
                method: .post,
                body: body,
                responseProcessor: responseProcessor
            )
            .map(updateToken)
    }
    
    func registerPatient(
        personDetails: PersonDetails,
        passport: Passport,
        insurance: Insurance,
        address: Address,
        credentials: Credentials
    ) -> Single<Void> {
        let registryInfo = RegistryInfo(
            personDetails: personDetails,
            passport: passport,
            insurance: insurance,
            address: address,
            credentials: credentials
        )
        
        let body = try? isoDateJsonEncoder.encode(registryInfo)
        
        networkService.dropToken()
        
        return networkService
            .fetchModel(
                path: "/patients/register",
                method: .post,
                body: body,
                responseProcessor: responseProcessor
            )
            .map(updateToken)
    }
    
    private func updateToken(_ token: Token) {
        networkService.updateToken(token)
    }
}
