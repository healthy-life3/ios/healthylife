//
//  AuthService.swift
//  HealthyLife
//
//  Created by Никита Гусев on 27.10.2021.
//

import Foundation
import RxSwift

// MARK: AuthServiceType
protocol AuthServiceType {
    func authorizeDoctor(using authInfo: AuthInfo) -> Single<Void>
    
    func authorizePatient(using authInfo: AuthInfo) -> Single<Void>
}

// MARK: - Auth Error
extension AuthService {
    enum Error: Swift.Error {
    }
}

// MARK: - Auth Response Processor
private final class AuthResponseProcessor: ResponseProcessorType {
    func extractError(from response: URLResponse) -> Error? {
        nil
    }
}

// MARK: AuthService
final class AuthService: AuthServiceType {
    private let networkService: NetworkServiceType
    private let responseProcessor: ResponseProcessorType
    
    init(networkService: NetworkServiceType) {
        self.networkService = networkService
        responseProcessor = AuthResponseProcessor()
    }
    
    func authorizeDoctor(using authInfo: AuthInfo) -> Single<Void> {
        networkService.dropToken()
        
        return networkService
            .fetchModel(
                path: "/doctors/auth",
                method: .post,
                additionalHeaders: [
                    Header.Authorization.basic(
                        user: authInfo.login.address,
                        password: authInfo.password
                    )
                ],
                responseProcessor: responseProcessor
            )
            .map(networkService.updateToken)
    }
    
    func authorizePatient(using authInfo: AuthInfo) -> Single<Void> {
        networkService.dropToken()
        
        return networkService
            .fetchModel(
                path: "/patients/auth",
                method: .post,
                additionalHeaders: [
                    Header.Authorization.basic(
                        user: authInfo.login.address,
                        password: authInfo.password
                    )
                ],
                responseProcessor: responseProcessor
            )
            .map(networkService.updateToken)
    }
}
