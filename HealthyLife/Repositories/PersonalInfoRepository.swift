//
//  PersonalInfoRepository.swift
//  HealthyLife
//
//  Created by Никита Гусев on 22.12.2021.
//

import Foundation
import RxCocoa

protocol PersonalInfoRepositoryType {
    var patientInfo: Driver<PatientPersonalInfo> { get }
    var doctorInfo: Driver<DoctorPersonalInfo> { get }
}

final class PersonalInfoRepository: PersonalInfoRepositoryType {
    private let networkService: NetworkServiceType
    private let responseProcessor: ResponseProcessorType
    
    init(networkService: NetworkServiceType) {
        self.networkService = networkService
        responseProcessor = PersonalInfoResponseProcessor()
    }
    
    var patientInfo: Driver<PatientPersonalInfo> {
        networkService.fetchModel(
            path: "/patients/profile",
            method: .get,
            responseProcessor: responseProcessor,
            decoder: yearMonthDayJsonDecoder
        )
        .asDriver(onErrorRecover: processError)
    }
    
    var doctorInfo: Driver<DoctorPersonalInfo> {
        networkService.fetchModel(
            path: "/doctors/profile",
            method: .get,
            responseProcessor: responseProcessor,
            decoder: yearMonthDayJsonDecoder
        )
        .asDriver(onErrorRecover: processError)
    }
}

// MARK: - PersonalInfo Error
extension PersonalInfoRepository {
    enum Error: Swift.Error {
    }
}

// MARK: - PersonalInfo Response Processor
private final class PersonalInfoResponseProcessor: ResponseProcessorType {
    func extractError(from response: URLResponse) -> Error? {
        nil
    }
}

// MARK: - Private methods
private extension PersonalInfoRepository {
    private func processError(_ error: Swift.Error) -> Driver<PatientPersonalInfo> {
        print(error)
        
        return .empty()
    }
    
    private func processError(_ error: Swift.Error) -> Driver<DoctorPersonalInfo> {
        print(error)
         
        return .empty()
    }
}
