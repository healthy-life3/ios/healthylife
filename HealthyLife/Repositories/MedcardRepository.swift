//
//  MedcardRepository.swift
//  HealthyLife
//
//  Created by Никита Гусев on 21.12.2021.
//

import Foundation
import RxSwift
import RxCocoa

protocol MedcardRepositoryType {
    var medicalRecords: Driver<[MedicalRecord]> { get }
}

final class MedcardRepository: MedcardRepositoryType {
    private let networkService: NetworkServiceType
    private let responseProcessor: ResponseProcessorType
    
    init(networkService: NetworkServiceType) {
        self.networkService = networkService
        responseProcessor = MedcardResponseProcessor()
    }
    
    var medicalRecords: Driver<[MedicalRecord]> {
        networkService.fetchModel(
            path: "/patients/medical-records",
            method: .get,
            responseProcessor: responseProcessor,
            decoder: baseIsoDateJsonDecoder
        )
        .asDriver(onErrorRecover: processError)
    }
}

// MARK: - Medcard Error
extension MedcardRepository {
    enum Error: Swift.Error {
    }
}

// MARK: - Medcard Response Processor
private final class MedcardResponseProcessor: ResponseProcessorType {
    func extractError(from response: URLResponse) -> Error? {
        nil
    }
}

// MARK: - Private methods
private extension MedcardRepository {
    private func processError(_ error: Swift.Error) -> Driver<[MedicalRecord]> {
        .empty()
    }
}
