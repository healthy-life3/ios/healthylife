//
//  DoctorProfile.swift
//  HealthyLife
//
//  Created by Артём on 22.12.2021.
//

import SwiftUI
import RxSwift
import RxCocoa

class DoctorProfileModel: ObservableObject {
    
    @Published var personDetails = FormsState.PersonDetails()
    @Published var contactInfo = FormsState.ContactInfo()
    @Published var passport = FormsState.Passport()
    
    private let disposedBag = DisposeBag()
    
    init(repository: PersonalInfoRepositoryType) {
        repository.doctorInfo
            .drive(onNext: updateInfo)
            .disposed(by: disposedBag)
    }
    
    private func updateInfo(_ info: DoctorPersonalInfo) {
        let personal = FormStateMapper.convertFromServiceEntity(info.personDetails)
        personDetails = personal.0
        contactInfo = personal.1
        passport = FormStateMapper.convertFromServiceEntity(info.passport)
    }
}

struct DoctorProfile: View {
    
    @ObservedObject
    var model: DoctorProfileModel
    
    var body: some View {
        ZStack {
            Color.background
                .ignoresSafeArea()
            ScrollView {
                VStack(spacing: 32) {
                    PersonalDetailsForm(state: model.personDetails)
                    ContactInfoForm(state: model.contactInfo)
                    PassportForm(state: model.passport)
                }
                .padding()
                .disabled(true)
            }
        }
        .navigationTitle("Профиль")
    }
}
