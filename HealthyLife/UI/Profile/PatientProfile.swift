//
//  PatientProfile.swift
//  HealthyLife
//
//  Created by Артём on 22.12.2021.
//

import SwiftUI
import RxSwift
import RxCocoa

class PatientProfileModel: ObservableObject {
    
    @Published var personDetails = FormsState.PersonDetails()
    @Published var contactInfo = FormsState.ContactInfo()
    @Published var passport = FormsState.Passport()
    @Published var insurance = FormsState.Insurance()
    @Published var address = FormsState.Address()
    
    private let coordinator: AppCoordinator
    private let disposedBag = DisposeBag()
    
    init(repository: PersonalInfoRepositoryType, coordinator: AppCoordinator) {
        self.coordinator = coordinator
        
        repository.patientInfo
            .drive(onNext: updateInfo)
            .disposed(by: disposedBag)
    }
    
    private func updateInfo(_ info: PatientPersonalInfo) {
        let personal = FormStateMapper.convertFromServiceEntity(info.personDetails)
        personDetails = personal.0
        contactInfo = personal.1
        passport = FormStateMapper.convertFromServiceEntity(info.passport)
        insurance = FormStateMapper.convertFromServiceEntity(info.insurance)
        address = FormStateMapper.convertFromServiceEntity(info.address)
        
        prepareAddress()
    }
    
    private func prepareAddress() {
        if !address.street.isEmpty {
            address.street = "Улица \(address.street)"
        }
        if !address.city.isEmpty {
            address.city = "Город \(address.city)"
        }
        if !address.house.isEmpty {
            address.house = "Дом \(address.house)"
        }
        if !address.apartment.isEmpty {
            address.apartment = "Квартира \(address.apartment)"
        }
    }
    
    func showRecords() {
        coordinator.root = .records
    }
}

struct PatientProfile: View {
    
    @ObservedObject
    var model: PatientProfileModel
    
    var body: some View {
        ZStack {
            Color.background
                .ignoresSafeArea()
            ScrollView {
                VStack(spacing: 32) {
                    PersonalDetailsForm(state: model.personDetails)
                    ContactInfoForm(state: model.contactInfo)
                    PassportForm(state: model.passport)
                    InsuranceForm(state: model.insurance)
                    AddressForm(state: model.address)
                }
                .padding()
                .disabled(true)
            }
        }
        .navigationTitle("Профиль")
        .toolbar {
            Button("Медкарта") {
                model.showRecords()
            }
        }
    }
}
