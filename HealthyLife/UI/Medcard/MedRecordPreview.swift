//
//  MedRecordPreview.swift
//  HealthyLife
//
//  Created by Артём on 22.12.2021.
//

import SwiftUI

class MedRecordPreviewModel: ObservableObject {
    var id: UUID
    @Published var title: String
    @Published var time: String
    @Published var source: String
    @Published var kind: MedicalRecord.Kind
    
    init(record: MedicalRecord) {
        id = record.recordId
        title = record.title
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        time = dateFormatter.string(from: record.date)
        
        kind = record.kind
        switch record.kind {
        case .lab:
            source = record.medicalOrganization
        case .visit:
            let doc = record.doctor
            let initials = "\(doc.firstName.first!). \(doc.middleName.first!)."
            source = "\(doc.lastName) \(initials)"
        }
    }
}

extension MedRecordPreviewModel: Identifiable { }

struct MedRecordPreview: View {
    
    @ObservedObject
    var model: MedRecordPreviewModel
    
    private var iconImage: Image {
        switch model.kind {
        case .lab:
            return Image("icon-lab")
        case .visit:
            return Image("icon-visit")
        }
    }
    
    var icon: some View {
        ZStack {
            Color.accent
            iconImage.padding(4)
        }
        .frame(width: 40, height: 40)
        .cornerRadius(8)
    }
    
    var body: some View {
        HStack(spacing: 0) {
            icon
                .padding()
            VStack(alignment: .leading, spacing: 8) {
                Text(model.title)
                Text(model.source)
                    .font(.system(size: 15))
                    .foregroundColor(.secondGray)
            }
            .padding(.vertical, 8)
            Spacer()
            
            Text(model.time)
                .padding()
                .foregroundColor(.secondGray)
        }
        .background(Color.lightBackground)
        .cornerRadius(16)
    }
}

struct MedRecordPreview_Previews: PreviewProvider {
    static var previews: some View {
        let doctor = MedicalRecord.Doctor(lastName: "Смирнов",
                                          firstName: "Иван",
                                          middleName: "Олегович")
        let record = MedicalRecord(recordId: UUID(),
                                   kind: .lab,
                                   title: "Биохимический анализ",
                                   date: Date(),
                                   doctor: doctor,
                                   medicalOrganization: "Инвитро")
        
        ZStack {
            Color.background
                .ignoresSafeArea()
            
            MedRecordPreview(model: MedRecordPreviewModel(record: record))
                .frame(maxWidth: .infinity, maxHeight: 70)
        }
    }
}
