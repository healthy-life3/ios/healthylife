//
//  MedRecordInfo.swift
//  HealthyLife
//
//  Created by Артём on 22.12.2021.
//

import SwiftUI

class MedRecordInfoModel: ObservableObject {
    @Published var title: String
    @Published var kind: String
    @Published var date: String
    @Published var time: String
    @Published var clinic: String
    @Published var doctor: String
    
    init(record: MedicalRecord) {
        title = record.title
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        time = dateFormatter.string(from: record.date)
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        date = dateFormatter.string(from: record.date)
        
        switch record.kind {
        case .visit:
            kind = "Прием"
        case .lab:
            kind = "Анализ"
        }
        
        let doc = record.doctor
        doctor = "\(doc.lastName) \(doc.firstName) \(doc.middleName)"
        clinic = record.medicalOrganization
    }
}

struct MedRecordInfo: View {
    
    @ObservedObject
    var model: MedRecordInfoModel
    
    func property(titled: String, value: String) -> some View {
        VStack(alignment: .leading, spacing: 0) {
            Text(titled)
                .font(.system(size: 12))
                .foregroundColor(Color.secondGray)
            Text(value)
        }
    }
    
    var info: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text(model.title)
                .font(.system(size: 24))
            
            VStack(alignment: .leading, spacing: 8) {
                property(titled: "Тип", value: model.kind)
                Divider()
                property(titled: "Дата", value: model.date)
                Divider()
                property(titled: "Время", value: model.time)
                Divider()
                property(titled: "Клиника", value: model.clinic)
                Divider()
                property(titled: "Врач", value: model.doctor)
            }
            .padding()
            .background(Color.lightBackground)
            .cornerRadius(16)
        }
    }
    
    var body: some View {
        ZStack {
            Color.background
                .ignoresSafeArea()
            
            VStack {
                info
                    .padding()
                
                Spacer()
            }
        }
    }
}

struct MedRecordInfo_Previews: PreviewProvider {
    static var previews: some View {
        let doctor = MedicalRecord.Doctor(lastName: "Смирнов",
                                          firstName: "Иван",
                                          middleName: "Олегович")
        let record = MedicalRecord(recordId: UUID(),
                                   kind: .lab,
                                   title: "Биохимический анализ",
                                   date: Date(),
                                   doctor: doctor,
                                   medicalOrganization: "Инвитро")
        
        MedRecordInfo(model: MedRecordInfoModel(record: record))
        
    }
}
