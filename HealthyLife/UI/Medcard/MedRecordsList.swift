//
//  MedRecordsList.swift
//  HealthyLife
//
//  Created by Артём on 22.12.2021.
//

import SwiftUI
import RxSwift
import RxCocoa

class MedRecordsListModel: ObservableObject {
    @Published
    var records: [MedicalRecord] = []
    
    private let coordinator: AppCoordinator
    private let disposedBag = DisposeBag()
    
    init(repository: MedcardRepositoryType, coordinator: AppCoordinator) {
        self.coordinator = coordinator
        
        repository.medicalRecords
            .drive(onNext: updateRecords)
            .disposed(by: disposedBag)
    }
    
    var recordsSections: [Section] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM"
        
        let dict = Dictionary(grouping: records) { record -> Date in
            let dateComponents = Calendar.current.dateComponents([.year, .month, .day],
                                                                 from: record.date)
            return Calendar.current.date(from: dateComponents)!
        }
        
        return dict
            .sorted { $0.key > $1.key }
            .map { (date, records) in
                let models = records
                    .map { (MedRecordPreviewModel(record: $0),
                            MedRecordInfoModel(record: $0))
                    }
                return Section(title: dateFormatter.string(from: date),
                               models: models)
            }
    }
    
    struct Section: Identifiable {
        var title: String
        var models: [(MedRecordPreviewModel, MedRecordInfoModel)]
        
        var id: String {
            title
        }
    }
    
    func showProfile() {
        coordinator.root = .patientProfile
    }
    
    private func updateRecords(records: [MedicalRecord]) {
        self.records = records
    }
}

struct MedRecordsList: View {
    @ObservedObject
    var model: MedRecordsListModel
    
    var body: some View {
        
        List {
            ForEach(model.recordsSections) { section in
                Section(header: Text(section.title).font(.system(size: 14))) {
                    ForEach(section.models, id: \.1.title) { p in
                        NavigationLink(destination: MedRecordInfo(model: p.1)) {
                            MedRecordPreview(model: p.0)
                        }
                        
                    }
                    .listRowInsets(.init(top: 8, leading: 0, bottom: 8, trailing: 0))
                    .listRowBackground(Color.clear)
                    .listRowSeparator(.hidden)
                }
            }
        }
        .navigationTitle("Медкарта")
        .toolbar {
            Button("Профиль") {
                model.showProfile()
            }
        }
    }
}
