//
//  WelcomeScreen.swift
//  HealthyLife
//
//  Created by Артём on 08.11.2021.
//

import SwiftUI

struct WelcomeScreen: View {
    
    enum Constants {
        static let hintMessage = "Добро пожаловать в систему по управлению медицинскими картами. Выберите роль, в качестве которой хотите продолжить:"
    }

    var viewModel: WelcomeScreenViewModel

    init(viewModel: WelcomeScreenViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ZStack {
            Color.background
                .ignoresSafeArea()
            
            VStack {
                Spacer()
                Text("Healthy Life")
                    .font(.system(size: 38, weight: .bold))
                Spacer()
                Text(Constants.hintMessage)
                    .frame(width: 300)
                    .multilineTextAlignment(.center)
                Spacer()
                VStack(spacing: 0) {
                    AppButton(title: "Пациент", minWidth: 100) {
                        viewModel.selectPatientRole()
                    }
                    .padding(.vertical, 8)
                    AppButton(title: "Врач", minWidth: 100) {
                        viewModel.selectDoctorRole()
                    }
                    .padding(.vertical, 8)
                }
                Spacer()
                Spacer()
            }
        }
    }
}
