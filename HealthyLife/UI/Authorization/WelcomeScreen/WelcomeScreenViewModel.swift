//
//  WelcomeScreenViewModel.swift
//  HealthyLife
//
//  Created by Артём on 08.11.2021.
//

import Foundation

class WelcomeScreenViewModel {
    private let coordinator: WelcomeScreenCoordinator
    
    init(coordinator: WelcomeScreenCoordinator) {
        self.coordinator = coordinator
    }
    
    func selectDoctorRole() {
        coordinator.showDoctorSingIn()
    }
    
    func selectPatientRole() {
        coordinator.showPatientSingIn()
    }
}

protocol WelcomeScreenCoordinator {
    func showDoctorSingIn()
    func showPatientSingIn()
}
