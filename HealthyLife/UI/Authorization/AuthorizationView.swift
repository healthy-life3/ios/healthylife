//
//  AuthorizationView.swift
//  HealthyLife
//
//  Created by Артём on 08.11.2021.
//

import SwiftUI
import Swinject

struct AuthorizationView: View {
    @ObservedObject
    private var coordinator: AuthorizationCoordinator

    private let welcomeScreen: WelcomeScreen
    private let doctorSingInView: SingInView<DoctorSignInViewModel>
    private let patientSingInView: SingInView<PatientSignInViewModel>
    private let doctorSingUpView: DoctorSingUp
    private let patientSingUpView: PatientSingUp
    
    init(resolver: Container) {
        self.coordinator = resolver.resolve()
        
        welcomeScreen = WelcomeScreen(viewModel: resolver.resolve())
        doctorSingInView = SingInView(title: "Вход врача", viewModel: resolver.resolve())
        patientSingInView = SingInView(title: "Вход пациента", viewModel: resolver.resolve())
        doctorSingUpView = DoctorSingUp(viewModel: resolver.resolve())
        patientSingUpView = PatientSingUp(viewModel: resolver.resolve())
    }
    
    var body: some View {
        switch coordinator.root {
        case .welcomeScreen:
            welcomeScreen
        case .doctorSingIn:
            doctorSingInView
        case .doctorSingUp:
            doctorSingUpView
        case .patientSingIn:
            patientSingInView
        case .patientSingUp:
            patientSingUpView
        }
    }
}
