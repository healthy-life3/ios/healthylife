//
//  DoctorSingInViewModel.swift
//  HealthyLife
//
//  Created by Артём on 08.11.2021.
//

import Foundation
import RxSwift
import RxCocoa

class DoctorSignInViewModel: SignInViewModelProtocol {
    @Published var state = FormsState.LogIn()
    
    private let appCoordinator: AppCoordinator
    private let coordinator: DoctorSignInCoordinator
    private let authService: AuthServiceType
    
    private let disposeBag = DisposeBag()
    
    init(coordinator: DoctorSignInCoordinator,
         authService: AuthServiceType,
         appCoordinator: AppCoordinator) {
        
        self.coordinator = coordinator
        self.authService = authService
        self.appCoordinator = appCoordinator
    }
    
    func goBack() {
        coordinator.showWelcomeScreen()
    }
    
    func signIn() {
        let authInfo = FormStateMapper.convertToServiceEntity(state)
        
        authService.authorizeDoctor(using: authInfo)
            .asDriver(onErrorRecover: processError)
            .drive(onNext: navigateToDoctorProfile)
            .disposed(by: disposeBag)
    }
    
    func selectSignUp() {
        coordinator.showDoctorSignUp()
    }
    
    private func processError(_ error: Error) -> Driver<Void> {
        print("Sign in error: ", error)
        
        return .never()
    }
    
    private func navigateToDoctorProfile() {
        appCoordinator.root = .doctorProfile
    }
}

protocol DoctorSignInCoordinator {
    func showWelcomeScreen()
    func showDoctorSignUp()
}
