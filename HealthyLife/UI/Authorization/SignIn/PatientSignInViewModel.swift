//
//  PatientSingInViewModel.swift
//  HealthyLife
//
//  Created by Артём on 08.11.2021.
//

import Foundation
import RxSwift
import RxCocoa

class PatientSignInViewModel: SignInViewModelProtocol {
    @Published var state = FormsState.LogIn()
    
    private let appCoordinator: AppCoordinator
    private let coordinator: PatientSingInCoordinator
    private let authService: AuthServiceType
    
    private let disposeBag = DisposeBag()
    
    init(coordinator: PatientSingInCoordinator,
         authService: AuthServiceType,
         appCoordinator: AppCoordinator) {
        self.coordinator = coordinator
        self.authService = authService
        self.appCoordinator = appCoordinator
    }
    
    func goBack() {
        coordinator.showWelcomeScreen()
    }
    
    func signIn() {
        let authInfo = FormStateMapper.convertToServiceEntity(state)
        
        authService.authorizePatient(using: authInfo)
            .asDriver(onErrorRecover: processError)
            .drive(onNext: navigateToMedicalRecords)
            .disposed(by: disposeBag)
    }
    
    func selectSignUp() {
        coordinator.showPatientSignUp()
    }
    
    private func processError(_ error: Error) -> Driver<Void> {
        print("Sign in error: ", error)
        
        return .never()
    }
    
    private func navigateToMedicalRecords() {
        appCoordinator.root = .records
    }
}

protocol PatientSingInCoordinator {
    func showWelcomeScreen()
    func showPatientSignUp()
}
