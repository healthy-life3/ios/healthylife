//
//  DoctorSingIn.swift
//  HealthyLife
//
//  Created by Артём on 08.11.2021.
//

import SwiftUI

protocol SignInViewModelProtocol: ObservableObject {
    var state: FormsState.LogIn { get }
    func goBack()
    func signIn()
    func selectSignUp()
}

struct SingInView<ViewModel: SignInViewModelProtocol>: View {
    let title: String
    
    @ObservedObject
    var viewModel: ViewModel
    
    var body: some View {
        ZStack {
            Color.background
                .ignoresSafeArea()
            
            VStack {
                Spacer()
                LogInForm(state: viewModel.state)
                Spacer()
                AppButton(title: "Войти") {
                    viewModel.signIn()
                }
                .padding()
                Button("Регистрация") {
                    viewModel.selectSignUp()
                }
                .padding(.bottom, 24)
            }
            .padding(.horizontal, 16)
            .navigationTitle(title)
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button("Вернуться") {
                        viewModel.goBack()
                    }
                }
            }
        }
    }
}
