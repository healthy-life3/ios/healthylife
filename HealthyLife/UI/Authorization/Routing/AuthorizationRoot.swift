//
//  AuthorizationRoot.swift
//  HealthyLife
//
//  Created by Артём on 08.11.2021.
//

import Foundation

enum AuthorizationRoot {
    case welcomeScreen
    case doctorSingIn
    case doctorSingUp
    case patientSingIn
    case patientSingUp
}
