//
//  AuthorizationCoordinator.swift
//  HealthyLife
//
//  Created by Артём on 08.11.2021.
//

import Foundation
import SwiftUI

class AuthorizationCoordinator: ObservableObject {
    @Published private(set) var root: AuthorizationRoot = .welcomeScreen
    
    func showWelcomeScreen() {
        root = .welcomeScreen
    }
    func showDoctorSingIn() {
        root = .doctorSingIn
    }
    func showPatientSingIn() {
        root = .patientSingIn
    }
    func showDoctorSignUp() {
        root = .doctorSingUp
    }
    func showPatientSignUp() {
        root = .patientSingUp
    }
}

extension AuthorizationCoordinator: WelcomeScreenCoordinator { }

extension AuthorizationCoordinator: DoctorSignInCoordinator { }

extension AuthorizationCoordinator: PatientSingInCoordinator { }

extension AuthorizationCoordinator: DoctorSignUpCoordinator { }

extension AuthorizationCoordinator: PatientSingUpCoordinator { }
