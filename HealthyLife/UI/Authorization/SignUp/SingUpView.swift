//
//  DoctorSingUpView.swift
//  HealthyLife
//
//  Created by Артём on 31.10.2021.
//

import SwiftUI

protocol FormsCoordinator {
    var canGoForward: Bool { get }
    var canGoBack: Bool { get }
    func goForward()
    func goBack()
}

struct SingUpView<FormsView>: View where FormsView: View & Validatable {
    let forms: FormsView
    let formsCoordinator: FormsCoordinator
    let onSingUp: () -> Void
    
    @State var isAlertPresented = false
    
    init(formsCoordinator: FormsCoordinator,
         formsContent: FormsView,
         onSingUp: @escaping () -> Void) {
        
        self.formsCoordinator = formsCoordinator
        self.forms = formsContent
        self.onSingUp = onSingUp
    }

    @ViewBuilder
    var button: some View {
        if(formsCoordinator.canGoForward) {
            AppButton(title: "Далее") {
                if forms.isValid {
                    formsCoordinator.goForward()
                } else {
                    isAlertPresented = true
                }
            }
        } else {
            AppButton(title: "Зарегистрироваться") {
                if forms.isValid {
                    onSingUp()
                } else {
                    isAlertPresented = true
                }
            }
        }
    }
    
    var body: some View {
        ZStack {
            Color.background
                .ignoresSafeArea()
            
            ScrollView {
                VStack(spacing: 8) {
                    forms
                        .padding(.horizontal, 4)
                        .padding(.top, 16)
                    
                    button
                        .padding(.vertical, 24)
                }
            }
            .padding(.horizontal, 16)
            .navigationTitle("Регистрация")
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button("Вернуться") {
                        formsCoordinator.goBack()
                    }
                }
            }
        }
        .alert(isPresented: $isAlertPresented) {
            Alert(title: Text("Неверные данные"),
                  message: Text("Заполните все поля корректной информацией прежде чем перейти дальше"),
                  dismissButton: .default(Text("Ок")))
        }

    }
}
