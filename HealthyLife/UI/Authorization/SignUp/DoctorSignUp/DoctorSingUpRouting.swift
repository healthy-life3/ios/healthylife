//
//  DoctorSingUpCoordinator.swift
//  HealthyLife
//
//  Created by Артём on 31.10.2021.
//

import SwiftUI

enum DoctorSingUpRoot {
    case doctorInfo
    case passport
    case password
}

class DoctorSingUpRouting: ObservableObject {
    
    @Published
    var root: DoctorSingUpRoot = .doctorInfo
    
    var canGoForward: Bool {
        root != .password
    }
    
    var canGoBack: Bool {
        root != .doctorInfo
    }
    
    private var nextView: DoctorSingUpRoot? {
        switch root {
        case .doctorInfo:
            return .passport
        case .passport:
            return .password
        case .password:
            return nil
        }
    }
    func goForward() -> DoctorSingUpRoot? {
        guard let nextView = nextView else {
            return nil
        }
        root = nextView
        return root
    }
    private var previousView: DoctorSingUpRoot? {
        switch root {
        case .doctorInfo:
            return nil
        case .passport:
            return .doctorInfo
        case .password:
            return .passport
        }
    }
    func goBack() -> DoctorSingUpRoot? {
        guard let previousView = previousView else {
            return nil
        }
        root = previousView
        return root
    }
}
