//
//  DoctorSingUpForms.swift
//  HealthyLife
//
//  Created by Артём on 07.11.2021.
//

import Foundation
import SwiftUI

struct DoctorSingUpForms: View, Validatable {
    @Binding
    private var root: DoctorSingUpRoot
    
    private let personalDetailsForm: PersonalDetailsForm
    private let contactInfoForm: ContactInfoForm
    private let passportForm: PassportForm
    private let passwordForm: PasswordForm
    
    init(root: Binding<DoctorSingUpRoot>, state: DoctorSingUpFormsState) {
        self._root = root
        
        personalDetailsForm = PersonalDetailsForm(state: state.personalDetails)
        contactInfoForm = ContactInfoForm(state: state.contactInfo)
        passportForm = PassportForm(state: state.passport)
        passwordForm = PasswordForm(state: state.password)
    }
    
    var body: some View {
        switch root {
        case .doctorInfo:
            personalDetailsForm
            contactInfoForm
        case .passport:
            passportForm
        case .password:
            passwordForm
        }
    }
    
    var isValid: Bool {
        switch root {
        case .doctorInfo:
            return personalDetailsForm.state.isValid &&
            contactInfoForm.state.isValid
        case .passport:
            return passportForm.state.isValid
        case .password:
            return passwordForm.state.isValid
        }
    }
}
