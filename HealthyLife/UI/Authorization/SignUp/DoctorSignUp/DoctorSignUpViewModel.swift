//
//  DoctorSingUpViewModel.swift
//  HealthyLife
//
//  Created by Артём on 01.11.2021.
//

import SwiftUI
import RxSwift
import RxCocoa

protocol DoctorSignUpCoordinator {
    func showDoctorSingIn()
}

class DoctorSignUpViewModel: ObservableObject {
    let state = DoctorSingUpFormsState()
    
    private let routing = DoctorSingUpRouting()
    @Published var root: DoctorSingUpRoot
    
    private let appCoordinator: AppCoordinator
    private let coordinator: DoctorSignUpCoordinator
    private let registryService: RegistryServiceType
    
    private let disposeBag = DisposeBag()
    
    init(
        appCoordinator: AppCoordinator,
        coordinator: DoctorSignUpCoordinator,
        registryService: RegistryServiceType
    ) {
        self.appCoordinator = appCoordinator
        self.coordinator = coordinator
        self.registryService = registryService
        
        root = routing.root
    }
}

extension DoctorSignUpViewModel: FormsCoordinator {
    var canGoForward: Bool {
        routing.canGoForward
    }
    
    var canGoBack: Bool {
        routing.canGoBack
    }
    
    func goForward() {
        guard let nextRoot = routing.goForward() else { return }
        root = nextRoot
    }
    
    func goBack() {
        guard let previousRoot = routing.goBack() else {
            coordinator.showDoctorSingIn()
            return
        }
        root = previousRoot
    }
    
    func signUp() {
        let personDetails = FormStateMapper.convertToServiceEntity(
            state.personalDetails,
            contacts: state.contactInfo
        )
        let passport = FormStateMapper.convertToServiceEntity(state.passport)
        let credentials = FormStateMapper.convertToServiceEntity(state.password)
        
        registryService.registerDoctor(
            personDetails: personDetails,
            passport: passport,
            credentials: credentials
        )
        .asDriver(onErrorRecover: processError)
        .drive(onNext: navigateToDoctorProfile)
        .disposed(by: disposeBag)
    }
    
    private func processError(_ error: Error) -> Driver<Void> {
        print("Sign up error: ", error)
        
        return .never()
    }
    
    private func navigateToDoctorProfile() {
        appCoordinator.root = .doctorProfile
    }
}

struct DoctorSingUpFormsState {
    let personalDetails = FormsState.PersonDetails()
    let contactInfo = FormsState.ContactInfo()
    let passport = FormsState.Passport()
    let password = FormsState.Password()
}
