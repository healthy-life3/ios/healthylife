//
//  DoctorSingUp.swift
//  HealthyLife
//
//  Created by Артём on 07.11.2021.
//

import SwiftUI

struct DoctorSingUp: View {
    
    @ObservedObject
    private var viewModel: DoctorSignUpViewModel
    
    init(viewModel: DoctorSignUpViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        SingUpView(formsCoordinator: viewModel,
                   formsContent: DoctorSingUpForms(root: $viewModel.root, state: viewModel.state)) {
            viewModel.signUp()
        }
    }
}
