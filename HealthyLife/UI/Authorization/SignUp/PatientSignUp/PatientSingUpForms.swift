//
//  PatientSingUpForms.swift
//  HealthyLife
//
//  Created by Артём on 07.11.2021.
//

import Foundation
import SwiftUI

struct PatientSingUpForms: View, Validatable {
    
    @Binding
    private var root: PatientSingUpRoot
    
    private let personalDetailsForm: PersonalDetailsForm
    private let contactInfoForm: ContactInfoForm
    private let passportForm: PassportForm
    private let insuranceForm: InsuranceForm
    private let addressForm: AddressForm
    private let passwordForm: PasswordForm
    
    init(root: Binding<PatientSingUpRoot>, state: PatientSingUpFormsState) {
        self._root = root
        
        personalDetailsForm = PersonalDetailsForm(state: state.personalDetails)
        contactInfoForm = ContactInfoForm(state: state.contactInfo)
        passportForm = PassportForm(state: state.passport)
        insuranceForm = InsuranceForm(state: state.insurance)
        addressForm = AddressForm(state: state.address)
        passwordForm = PasswordForm(state: state.password)
    }
    
    var body: some View {
        switch root {
        case .patientInfo:
            personalDetailsForm
            contactInfoForm
        case .passport:
            passportForm
            insuranceForm
        case .address:
            addressForm
        case .password:
            passwordForm
        }
    }
    
    var isValid: Bool {
        switch root {
        case .patientInfo:
            return personalDetailsForm.state.isValid &&
            contactInfoForm.state.isValid
        case .passport:
            return passportForm.state.isValid &&
            insuranceForm.state.isValid
        case .address:
            return addressForm.state.isValid
        case .password:
            return passwordForm.state.isValid
        }
    }
}
