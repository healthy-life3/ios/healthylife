//
//  PatientSingUp.swift
//  HealthyLife
//
//  Created by Артём on 07.11.2021.
//

import SwiftUI

struct PatientSingUp: View {
    
    @ObservedObject
    private var viewModel: PatientSingUpViewModel
    
    init(viewModel: PatientSingUpViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        SingUpView(formsCoordinator: viewModel,
                   formsContent: PatientSingUpForms(root: $viewModel.root, state: viewModel.state)) {
            viewModel.signUp()
        }
    }
}
