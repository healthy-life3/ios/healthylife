//
//  PatientSingUpViewModel.swift
//  HealthyLife
//
//  Created by Артём on 01.11.2021.
//

import SwiftUI
import RxSwift
import RxCocoa

protocol PatientSingUpCoordinator {
    func showPatientSingIn()
}

class PatientSingUpViewModel: ObservableObject {
    let state = PatientSingUpFormsState()
    
    private let routing = PatientSingUpRouting()
    @Published var root: PatientSingUpRoot
    
    private let appCoordinator: AppCoordinator
    private let coordinator: PatientSingUpCoordinator
    private let registryService: RegistryServiceType
    
    private let disposeBag = DisposeBag()
    
    init(
        appCoordinator: AppCoordinator,
        coordinator: PatientSingUpCoordinator,
        registryService: RegistryServiceType
    ) {
        self.appCoordinator = appCoordinator
        self.coordinator = coordinator
        self.registryService = registryService
        root = routing.root
    }
}

extension PatientSingUpViewModel: FormsCoordinator {
    var canGoForward: Bool {
        routing.canGoForward
    }
    
    var canGoBack: Bool {
        routing.canGoBack
    }
    
    func goForward() {
        guard let nextRoot = routing.goForward() else {
            return
        }
        root = nextRoot
    }
    
    func goBack() {
        guard let previousRoot = routing.goBack() else {
            coordinator.showPatientSingIn()
            return
        }
        root = previousRoot
    }
    
    func signUp() {
        let personDetails = FormStateMapper.convertToServiceEntity(
            state.personalDetails,
            contacts: state.contactInfo
        )
        let passport = FormStateMapper.convertToServiceEntity(state.passport)
        let insurance = FormStateMapper.convertToServiceEntity(state.insurance)
        let address = FormStateMapper.convertToServiceEntity(state.address)
        let credentials = FormStateMapper.convertToServiceEntity(state.password)
        
        registryService.registerPatient(
            personDetails: personDetails,
            passport: passport,
            insurance: insurance,
            address: address,
            credentials: credentials
        )
        .asDriver(onErrorRecover: processError)
        .drive(onNext: navigateToMedicalRecords)
        .disposed(by: disposeBag)
    }
    
    private func processError(_ error: Error) -> Driver<Void> {
        print("Sign up error: ", error)
        
        return .never()
    }
    
    private func navigateToMedicalRecords() {
        appCoordinator.root = .records
    }
}

struct PatientSingUpFormsState {
    let personalDetails = FormsState.PersonDetails()
    let contactInfo = FormsState.ContactInfo()
    let passport = FormsState.Passport()
    let insurance = FormsState.Insurance()
    let address = FormsState.Address()
    let password = FormsState.Password()
}
