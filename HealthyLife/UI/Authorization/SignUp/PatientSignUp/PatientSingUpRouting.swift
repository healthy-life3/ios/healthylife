//
//  PatientSingUpCoordinator.swift
//  HealthyLife
//
//  Created by Артём on 31.10.2021.
//

import SwiftUI

enum PatientSingUpRoot {
    case patientInfo
    case passport
    case address
    case password
}

class PatientSingUpRouting: ObservableObject {
    
    @Published
    var root: PatientSingUpRoot = .patientInfo
    
    var canGoForward: Bool {
        root != .password
    }
    var canGoBack: Bool {
        root != .patientInfo
    }
    
    private var nextView: PatientSingUpRoot? {
        switch root {
        case .patientInfo:
            return .passport
        case .passport:
            return .address
        case .address:
            return .password
        case .password:
            return nil
        }
    }
    func goForward() -> PatientSingUpRoot? {
        guard let nextView = nextView else {
            return nil
        }
        root = nextView
        return root
    }
    private var previousView: PatientSingUpRoot? {
        switch root {
        case .patientInfo:
            return nil
        case .passport:
            return .patientInfo
        case .address:
            return .passport
        case .password:
            return .address
        }
    }
    func goBack() -> PatientSingUpRoot? {
        guard let previousView = previousView else {
            return nil
        }
        root = previousView
        return root
    }
}
