//
//  AppViewConstants.swift
//  HealthyLife
//
//  Created by Артём on 29.10.2021.
//

import Foundation
import SwiftUI

enum AppFieldConstants {
    static let cornerRadius: CGFloat = 16
    private static let backgroundRect = RoundedRectangle(cornerRadius: cornerRadius)
    static let fieldStroke: some View = backgroundRect.stroke(Color.tertiaryGray, lineWidth: 1)
    static let fieldBackground: some View = backgroundRect.fill(Color.lightBackground)
}
