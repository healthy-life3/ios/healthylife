//
//  AppDatePicker.swift
//  HealthyLife
//
//  Created by Артём on 29.10.2021.
//

import SwiftUI

struct AppDatePicker: View {
    let title: String
    @Binding var date: Date
    
    var body: some View {
        HStack {
            Text(title)
                .foregroundColor(.secondGray)
            Spacer()
            DatePicker(title,
                       selection: $date,
                       in: ...Date(),
                       displayedComponents: .date)
                .labelsHidden()
        }
        .padding(11)
        .background(AppFieldConstants.fieldBackground)
        .overlay(AppFieldConstants.fieldStroke)
    }
}

struct AppDatePicker_Previews: PreviewProvider {
    
    @State
    static var date = Date()
    
    static var previews: some View {
        AppDatePicker(title: "Дата рождения", date: $date)
            .padding()
    }
}
