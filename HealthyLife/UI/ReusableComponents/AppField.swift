//
//  FieldView.swift
//  HealthyLife
//
//  Created by Артём on 26.10.2021.
//

import SwiftUI

struct SecureAppField: View {
    let title: String
    @Binding var text: String
    
    init(title: String, text: Binding<String>) {
        self.title = title
        self._text = text
    }
    
    var body: some View {
        SecureField(title, text: $text)
            .padding()
            .background(AppFieldConstants.fieldBackground)
            .overlay(AppFieldConstants.fieldStroke)
    }
}

struct AppField: View {
    let title: String
    @Binding var text: String
    private let capitalization: UITextAutocapitalizationType
    
    init(title: String,
         text: Binding<String>,
         capitalization: UITextAutocapitalizationType = .none
    ) {
        self.title = title
        self._text = text
        self.capitalization = capitalization
    }
    
    var body: some View {
        TextField(title, text: $text)
            .padding()
            .background(AppFieldConstants.fieldBackground)
            .overlay(AppFieldConstants.fieldStroke)
            .autocapitalization(capitalization)
    }
}

struct AppField_Previews: PreviewProvider {
    @State static var text: String = ""
    
    static var previews: some View {
        AppField(title: "Title", text: $text)
            .padding()
    }
}
