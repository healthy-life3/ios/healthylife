//
//  AppPicker.swift
//  HealthyLife
//
//  Created by Артём on 29.10.2021.
//

import SwiftUI

typealias EnumType = RawRepresentable & CaseIterable & Hashable

struct AppPicker<E: EnumType>: View where E.RawValue == String,
                                          E.AllCases: RandomAccessCollection {
    let title: String
    @Binding var value: E

    var body: some View {
        HStack {
            Text(title)
                .foregroundColor(.secondGray)
            Spacer()
            Picker(title, selection: $value) {
                ForEach(E.allCases, id: \.self) {
                    Text($0.rawValue)
                }
            }
            .accentColor(.systemBlack)
            .scaleEffect(CGSize(width: 1.2, height: 1.2))
            .padding(.horizontal, 16)
            .padding(.vertical, 2)
            .background(RoundedRectangle(cornerRadius: 8).fill(Color.pickerColor))
        }
        .padding(11)
        .background(AppFieldConstants.fieldBackground)
        .overlay(AppFieldConstants.fieldStroke)
    }
}

struct AppPicker_Previews: PreviewProvider {
    
    enum Cases: String, CaseIterable {
        case first = "first-1"
        case second = "second-2"
    }
    
    @State
    static var val: Cases = .first
    
    static var previews: some View {
        AppPicker(title: "Picker", value: $val)
            .preferredColorScheme(/*@START_MENU_TOKEN@*/.dark/*@END_MENU_TOKEN@*/)
            .padding()
    }
}
