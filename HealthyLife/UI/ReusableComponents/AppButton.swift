//
//  SimpleButton.swift
//  HealthyLife
//
//  Created by Артём on 28.10.2021.
//

import SwiftUI

struct AppButton: View {
    
    let title: String
    let action: ()->Void
    
    let minWidth: CGFloat
    
    init(title: String,
         minWidth: CGFloat = 0,
         action: @escaping ()->Void) {
        self.title = title
        self.minWidth = minWidth
        self.action = action
    }
    
    var body: some View {
        Button(title) {
            action()
        }
        .frame(minWidth: minWidth)
        .padding()
        .background(Color.accent)
        .foregroundColor(.white)
        .cornerRadius(16)
    }
}

struct AppButton_Previews: PreviewProvider {
    static var previews: some View {
        AppButton(title: "Button") {
            print("Hi")
        }
    }
}
