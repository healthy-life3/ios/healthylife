//
//  AppTitledFieldsGroup.swift
//  HealthyLife
//
//  Created by Артём on 28.10.2021.
//

import SwiftUI

struct AppTitledFieldsGroup<Content: View>: View {
    let title: String
    let content: Content

    init(title: String, @ViewBuilder content: () -> Content) {
        self.title = title
        self.content = content()
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text(title)
                .bold()
                .foregroundColor(.secondGray)
            content
        }
    }
}

struct AppTitledFieldsGroup_Previews: PreviewProvider {
    static var previews: some View {
        AppTitledFieldsGroup(title: "Title") {
            Text("Hello")
        }
    }
}
