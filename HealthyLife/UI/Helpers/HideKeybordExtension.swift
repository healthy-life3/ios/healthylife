//
//  HideKeybord.swift
//  HealthyLife
//
//  Created by Артём on 01.11.2021.
//

import UIKit
import SwiftUI

extension View {
    func hideKeyboard() {
        let selector = #selector(UIResponder.resignFirstResponder)
        UIApplication.shared.sendAction(selector, to: nil, from: nil, for: nil)
    }
}
