//
//  ColorShemeExtension.swift
//  HealthyLife
//
//  Created by Артём on 26.10.2021.
//

import SwiftUI

extension Color {
    
    static let pickerColor = Color("Picker")
    
    static let lightBackground = Color("White-DarkGray")
    
    static let systemWhite = Color(UIColor.systemBackground)
    static let systemBlack = Color("SystemBlack")
    static let background = Color("LightGray-Black")
    static let accent = Color("AccentColor")
    
    static let secondGray = Color("SecondGray")
    static let tertiaryGray = Color("TertiaryGray")
}
