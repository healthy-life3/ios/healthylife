//
//  PassportForm.swift
//  HealthyLife
//
//  Created by Артём on 31.10.2021.
//

import SwiftUI

struct PassportForm: View {
    
    @ObservedObject
    var state: FormsState.Passport
    
    var body: some View {
        AppTitledFieldsGroup(title: "Паспортные данные") {
            AppField(title: "Серия", text: $state.series)
            AppField(title: "Номер", text: $state.number)
            AppField(title: "Кем выдан", text: $state.producer)
            AppDatePicker(title: "Дата выдачи", date: $state.produceDate)
        }
    }
}

struct PassportForm_Previews: PreviewProvider {
    static var previews: some View {
        PassportForm(state: FormsState.Passport())
    }
}
