//
//  ContactInfoForm.swift
//  HealthyLife
//
//  Created by Артём on 31.10.2021.
//

import SwiftUI

struct ContactInfoForm: View {
    
    @ObservedObject
    var state: FormsState.ContactInfo
    
    var body: some View {
        AppTitledFieldsGroup(title: "Контакты") {
            AppField(title: "Номер телефона", text: $state.phone)
            AppField(title: "Электронная почта", text: $state.email)
        }
    }
}

struct ContactInfoForm_Previews: PreviewProvider {
    static var previews: some View {
        ContactInfoForm(state: FormsState.ContactInfo())
    }
}
