//
//  LogInForm.swift
//  HealthyLife
//
//  Created by Артём on 08.11.2021.
//

import SwiftUI

struct LogInForm: View {
    
    @ObservedObject
    var state: FormsState.LogIn
    
    var body: some View {
        AppTitledFieldsGroup(title: "Логин") {
            AppField(title: "Электронная почта",
                     text: $state.login,
                     capitalization: .none)
        }
        AppTitledFieldsGroup(title: "Пароль") {
            SecureAppField(title: "Пароль", text: $state.password)
        }
    }
}

struct LogInForm_Previews: PreviewProvider {
    static var previews: some View {
        LogInForm(state: FormsState.LogIn())
    }
}
