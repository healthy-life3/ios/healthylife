//
//  AddressForm.swift
//  HealthyLife
//
//  Created by Артём on 31.10.2021.
//

import SwiftUI

struct AddressForm: View {
    
    @ObservedObject
    var state: FormsState.Address
    
    var body: some View {
        AppTitledFieldsGroup(title: "Адресс проживания") {
            AppField(title: "Область", text: $state.region)
            AppField(title: "Город", text: $state.city)
            AppField(title: "Улица", text: $state.street)
            AppField(title: "Номер дома", text: $state.house)
            AppField(title: "Номер квартиры", text: $state.apartment)
        }
    }
}

struct AddressForm_Previews: PreviewProvider {
    static var previews: some View {
        AddressForm(state: FormsState.Address())
    }
}
