//
//  InsuranceForm.swift
//  HealthyLife
//
//  Created by Артём on 07.11.2021.
//

import SwiftUI

struct InsuranceForm: View {
    
    @ObservedObject
    var state: FormsState.Insurance
    
    var body: some View {
        AppTitledFieldsGroup(title: "СНИЛС") {
            AppField(title: "СНИЛС", text: $state.number)
        }
    }
}

struct InsuranceForm_Previews: PreviewProvider {
    static var previews: some View {
        InsuranceForm(state: FormsState.Insurance())
    }
}
