//
//  StateModels.swift
//  HealthyLife
//
//  Created by Артём on 31.10.2021.
//

import Foundation

enum FormGender: String, CaseIterable {
    case unknown = "Не выбран"
    case male = "Мужчина"
    case female = "Женщина"
}

protocol Validatable {
    var isValid: Bool { get }
}

enum FormsState {
    class PersonDetails: ObservableObject, Validatable {
        @Published var lastName: String = ""
        @Published var firstName: String = ""
        @Published var middleName: String = ""
        @Published var gender: FormGender = .unknown
        @Published var birthDate: Date = Date()
        
        var isValid: Bool {
            !lastName.isEmpty &&
            !firstName.isEmpty &&
            !middleName.isEmpty &&
            gender != .unknown
        }
    }
    
    class ContactInfo: ObservableObject, Validatable {
        @Published var phone: String = ""
        @Published var email: String = ""
        
        var isValid: Bool {
            !phone.isEmpty &&
            !email.isEmpty
        }
    }
    
    class Passport: ObservableObject, Validatable {
        @Published var series: String = ""
        @Published var number: String = ""
        @Published var produceDate: Date = Date()
        @Published var producer: String = ""
        
        var isValid: Bool {
            !series.isEmpty &&
            !number.isEmpty &&
            !producer.isEmpty
        }
    }
    
    class Insurance: ObservableObject, Validatable {
        @Published var number: String = ""
        
        var isValid: Bool {
            !number.isEmpty
        }
    }
    
    class Address: ObservableObject, Validatable {
        @Published var region: String = ""
        @Published var city: String = ""
        @Published var street: String = ""
        @Published var house: String = ""
        @Published var apartment: String = ""
        
        var isValid: Bool {
            !region.isEmpty &&
            !city.isEmpty &&
            !street.isEmpty &&
            !house.isEmpty &&
            !apartment.isEmpty
        }
    }
    
    class Password: ObservableObject, Validatable {
        @Published var password: String = ""
        @Published var confirmation: String = ""
        
        var isValid: Bool {
            !password.isEmpty &&
            !confirmation.isEmpty &&
            password == confirmation
        }
    }
    
    class LogIn: ObservableObject, Validatable {
        @Published var login: String = ""
        @Published var password: String = ""
        
        var isValid: Bool {
            !login.isEmpty &&
            !password.isEmpty
        }
    }
}
