//
//  ConvertionToEntity.swift
//  HealthyLife
//
//  Created by Артём on 10.12.2021.
//

import Foundation

extension FormGender {
    func toServiceEntity() -> Gender {
        switch self {
        case .unknown:
            return .unknown
        case .male:
            return .male
        case .female:
            return .female
        }
    }
}

class FormStateMapper {
    static func convertToServiceEntity(_ personalDetails: FormsState.PersonDetails,
                                       contacts: FormsState.ContactInfo
    ) -> PersonDetails {
        let phoneNumber = Phone(fullNumber: contacts.phone,
                                countryCode: String(contacts.phone.dropFirst().prefix(3)),
                                subscriberNumber: String(contacts.phone.dropFirst()))!
        let email = Email(address: contacts.email)!
        
        return PersonDetails(lastName: personalDetails.lastName,
                             firstName: personalDetails.firstName,
                             middleName: personalDetails.middleName,
                             gender: personalDetails.gender.toServiceEntity(),
                             birthday: personalDetails.birthDate,
                             phoneNumber: phoneNumber,
                             email: email)
    }
    
    static func convertFromServiceEntity(_ personDetails: PersonDetails
    ) -> (FormsState.PersonDetails, FormsState.ContactInfo) {
        let person = FormsState.PersonDetails()
        person.firstName = personDetails.firstName
        person.middleName = personDetails.middleName ?? ""
        person.lastName = personDetails.lastName
        person.birthDate = personDetails.birthday
        switch personDetails.gender {
        case .female:
            person.gender = .female
        case .male:
            person.gender = .male
        case .unknown:
            person.gender = .unknown
        }
        
        let contact = FormsState.ContactInfo()
        contact.email = personDetails.email.address
        contact.phone = personDetails.phoneNumber.fullNumber
        return (person, contact)
    }
    
    static func convertToServiceEntity(_ passport: FormsState.Passport) -> Passport {
        return Passport(documentSeries: passport.series,
                        documentNumber: passport.number,
                        dateOfIssue: passport.produceDate,
                        placeOfIssue: passport.producer)
    }
    
    static func convertFromServiceEntity(_ passportDetails: Passport) -> FormsState.Passport {
        let passport = FormsState.Passport()
        passport.series = passportDetails.documentSeries
        passport.number = passportDetails.documentNumber
        passport.producer = passportDetails.placeOfIssue
        passport.produceDate = passportDetails.dateOfIssue
        return passport
    }
    
    static func convertToServiceEntity(_ insurance: FormsState.Insurance) -> Insurance {
        return Insurance(snils: insurance.number)
    }
    
    static func convertFromServiceEntity(_ insuranceDetails: Insurance) -> FormsState.Insurance {
        let insurance = FormsState.Insurance()
        insurance.number = insuranceDetails.snils
        return insurance
    }
    
    static func convertToServiceEntity(_ address: FormsState.Address) -> Address {
        return Address(region: address.region,
                       city: address.city,
                       street: address.street,
                       houseNumber: address.house,
                       apartmentNumber: Int(address.apartment),
                       zipcode: "000000")
    }
    
    static func convertFromServiceEntity(_ addressDetails: Address) -> FormsState.Address {
        let address = FormsState.Address()
        address.region = addressDetails.region
        address.city = addressDetails.city
        address.street = addressDetails.street
        address.house = addressDetails.houseNumber
        
        if let apartment = addressDetails.apartmentNumber {
            address.apartment = "\(apartment)"
        }
        return address
    }
    
    static func convertToServiceEntity(_ password: FormsState.Password) -> Credentials {
        return Credentials(password: password.password)
    }
    
    static func convertToServiceEntity(_ loginInfo: FormsState.LogIn) -> AuthInfo {
        return AuthInfo(login: Email(address: loginInfo.login)!,
                        password: loginInfo.password)
    }
}
