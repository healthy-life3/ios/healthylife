//
//  UserInfoForm.swift
//  HealthyLife
//
//  Created by Артём on 29.10.2021.
//

import SwiftUI

struct PersonalDetailsForm: View {
    
    @ObservedObject
    var state: FormsState.PersonDetails
    
    var body: some View {
        AppTitledFieldsGroup(title: "Персональные данные") {
            AppField(title: "Фамилия", text: $state.lastName)
            AppField(title: "Имя", text: $state.firstName)
            AppField(title: "Отчество", text: $state.middleName)
            AppDatePicker(title: "Дата рождения", date: $state.birthDate)
            AppPicker(title: "Пол", value: $state.gender)
        }
    }
}

struct PersonalDetailsForm_Previews: PreviewProvider {
    static var previews: some View {
        PersonalDetailsForm(state: FormsState.PersonDetails())
    }
}
