//
//  PasswordForm.swift
//  HealthyLife
//
//  Created by Артём on 31.10.2021.
//

import SwiftUI

struct PasswordForm: View {
    
    @ObservedObject
    var state: FormsState.Password
    
    var body: some View {
        AppTitledFieldsGroup(title: "Новый пароль") {
            SecureAppField(title: "Пароль", text: $state.password)
        }
        AppTitledFieldsGroup(title: "Повторите пароль") {
            SecureAppField(title: "Подтверждение пароля", text: $state.confirmation)
        }
    }
}

struct PasswordForm_Previews: PreviewProvider {
    static var previews: some View {
        PasswordForm(state: FormsState.Password())
    }
}
