//
//  DependencyGraph.swift
//  HealthyLife
//
//  Created by Артём on 10.12.2021.
//

import Foundation

import Swinject
import SwinjectAutoregistration

extension Container {
    func registerBindings() -> Container {
        registerNetworkServices()
        authorizationView()
        app()
        return self
    }
}

class ResponseProcessorStub: ResponseProcessorType {
    func extractError(from response: URLResponse) -> Error? {
        return nil
    }
}

fileprivate extension Container {

    func registerNetworkServices() {
        register(NetworkService.self) { _ in
            NetworkService(baseUrl: URL(string: "http://34.71.172.16")!,
                           commonHeaders: [Header.ContentType.json],
                           responseProcessor: ResponseProcessorStub())
        }.inObjectScope(.container)
        bind(NetworkServiceType.self, to: NetworkService.self)
        
        autoregister(AuthServiceType.self, initializer: AuthService.init)
        autoregister(RegistryServiceType.self, initializer: RegistryService.init)
    }
    
    func authorizationView() {
        autoregister(AuthorizationCoordinator.self, initializer: AuthorizationCoordinator.init)
            .inObjectScope(.container)
        
        bind(WelcomeScreenCoordinator.self, to: AuthorizationCoordinator.self)
        bind(DoctorSignInCoordinator.self, to: AuthorizationCoordinator.self)
        bind(PatientSingInCoordinator.self, to: AuthorizationCoordinator.self)
        bind(DoctorSignUpCoordinator.self, to: AuthorizationCoordinator.self)
        bind(PatientSingUpCoordinator.self, to: AuthorizationCoordinator.self)
        
        autoregister(WelcomeScreenViewModel.self, initializer: WelcomeScreenViewModel.init)
        autoregister(PatientSingUpViewModel.self, initializer: PatientSingUpViewModel.init)
        autoregister(PatientSignInViewModel.self, initializer: PatientSignInViewModel.init)
        autoregister(DoctorSignUpViewModel.self, initializer: DoctorSignUpViewModel.init)
        autoregister(DoctorSignInViewModel.self, initializer: DoctorSignInViewModel.init)
    }
    
    func app() {
        autoregister(AppCoordinator.self, initializer: AppCoordinator.init)
            .inObjectScope(.container)
        
        autoregister(MedcardRepositoryType.self, initializer: MedcardRepository.init)
        autoregister(MedRecordsListModel.self, initializer: MedRecordsListModel.init)
        
        autoregister(PersonalInfoRepositoryType.self, initializer: PersonalInfoRepository.init)
        autoregister(PatientProfileModel.self, initializer: PatientProfileModel.init)
        autoregister(DoctorProfileModel.self, initializer: DoctorProfileModel.init)
    }
}

fileprivate extension Container {
    // swiftlint:disable force_cast
    func bind<S, T>(_ firstType: S.Type, to secondType: T.Type) {
        register(firstType) { resolver in
            let result: T = resolver.resolve(secondType)!
            return result as! S
        }
    }
}
