//
//  Container+Ext.swift
//  HealthyLife
//
//  Created by Артём on 10.12.2021.
//

import Swinject

extension Container {
    func resolve<T>() -> T {
        resolve(T.self)!
    }

    func resolve<T, A1>(_ argument: A1) -> T {
        resolve(T.self, argument: argument)!
    }
}
