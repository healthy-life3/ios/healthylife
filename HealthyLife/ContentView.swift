//
//  ContentView.swift
//  HealthyLife
//
//  Created by Артём on 25.10.2021.
//

import SwiftUI
import Swinject

enum AppRouting {
    case auth
    case records
    case doctorProfile
    case patientProfile
}

class AppCoordinator: ObservableObject {
    @Published
    var root: AppRouting = .auth
}

struct ContentView: View {
    
    private let dependencyResolver: Container
    
    @ObservedObject
    private var coordinator: AppCoordinator
    
    init(dependencyResolver: Container) {
        self.dependencyResolver = dependencyResolver
        coordinator = dependencyResolver.resolve()
    }
    
    var body: some View {
        NavigationView {
            switch coordinator.root {
            case .records:
                MedRecordsList(model: dependencyResolver.resolve())
            case .auth:
                AuthorizationView(resolver: dependencyResolver)
            case .doctorProfile:
                DoctorProfile(model: dependencyResolver.resolve())
            case .patientProfile:
                PatientProfile(model: dependencyResolver.resolve())
            }
        }
    }
}
